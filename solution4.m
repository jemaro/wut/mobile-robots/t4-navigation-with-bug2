function [forwBackVel, leftRightVel, rotVel, finish] = solution4(...
        pts, contacts, position, orientation, varargin)
    % Initialize regulator parameters
    MAX_VEL = 5;
    MAX_ROT_VEL = 10;
    KP_R = 2;
    KP_ROT_VEL = 10;
    KP_VEL = 20;
    % Initialize parameters
    wall_dist = 0.7;
    wall_angle = wrapToPi(0);
    sense = 1; % 1 = left, -1 = right
    smooth_edges = true;
    isdebug = false;
    tol = 1e-1;

    % Initialize the robot control variables (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    % Parameters

    % Parse input
    goal_pos = varargin{1};
    if length(varargin) > 1, sense = varargin{2}; end
    if length(varargin) > 2, isdebug = varargin{3}; end
    pos = position(1:2);
    att = orientation(3);

    % Finite State Machine initialization
    [GOAL, WALL] = assign_states();
    persistent state l qh dist_hit2goal just_hit axcart axpolar;
    % l: Line towards goal = [m b]; y = mx + b
    % qH: Hit point
    % dist_hit2goal: distance from the hit point to the goal
    % just_hit: boolean, whether the robot has just found a wall or not, used
    % to avoid false error conditions

    %% Initialization
    if isempty(state)
        state = GOAL;
        % Find the line l connecting initial and target position
        l(1) = (goal_pos(2) - pos(2)) / (goal_pos(1) - pos(1));
        l(2) = goal_pos(2) - l(1) * goal_pos(1);
        % Initialize the hit point
        qh = pos;
        dist_hit2goal = 0;
        % Initialize the plots
        if isdebug
            f1 = figure(1);
            clf(f1);
            axcart = axes(f1);
            plot(axcart, pos, 'k*');
            f2 = figure(2);
            axpolar = polaraxes(f2);
            polarplot(axpolar, 0, 0, 'k*');
        end

        disp('Start moving now')
    end

    %% Analyze LIDAR readings to check if robot is close to an obstacle
    % Convert pts to polar coordinates
    [theta, rho] = cart2pol(pts(1, :), pts(2, :));
    % There is a rotation between the robot coordinate frame and the
    % sensor pointing, we want 0 to be the center, the face of the
    % sensor
    theta = theta + pi / 2;
    % Identify wall orthogonal points by finding local minimums
    walls = islocalmin([5 rho 5], 'MinProminence', 0.2);
    % Obviously this minimums should be contacts, points that are
    % inside the lidar range
    walls = and(walls(2:end - 1), contacts);

    %% Compute the closest point ql to line l
    ql(1) = (pos(1) + l(1) * (pos(2) - l(2))) / (l(1)^2 + 1);
    ql(2) = ql(1) * l(1) + l(2);
    diff_ql = ql - pos;
    dist_line = norm(diff_ql);

    %% State changes
    % Always check goal as terminal condition
    diff_goal = goal_pos - pos;
    dist_goal = norm(diff_goal);
    goal_dir = angdiff(atan2(diff_goal(2), diff_goal(1)), att - pi / 2);

    if dist_goal < tol
        finish = true;
        return
    end

    % Other state changes that depend on the current state
    switch state
        case GOAL

            if any(rho(walls) <= wall_dist) % Obstacle reached
                state = WALL;
                qh = pos;
                just_hit = true;
                dist_hit2goal = dist_goal;
            end

        case WALL
            % Check if we are back again in the line
            if (dist_line < tol)
                % Check the LIDAR sensor in the direction of the goal
                [~, idx] = min(abs(angdiff(theta, goal_dir)));
                % Check that there is no obstacle in the direction of the goal
                % and that the goal is closer here
                if (rho(idx) > 2 * wall_dist) && (dist_goal < dist_hit2goal)
                    state = GOAL;
                elseif norm(qh - pos) < tol % Is it the starting point?

                    if ~just_hit
                        error('Goal is unreachable')
                    end

                else
                    just_hit = false;
                end

            end

        otherwise
            error('Unknown state %s.\n', state);
    end

    %% Finite State Machine code
    % The error and direction for the proportional controller will be computed
    % depending on the state
    switch state
        case GOAL
            % Compute the direction towards the closest point in the goal line
            r_dir = angdiff(atan2(diff_ql(2), diff_ql(1)), att - pi / 2);
            % Our logic is to treat the line as a wall to follow so we need to
            % check which is the direction of the line that leads to the goal
            sense = sign(angdiff(goal_dir, r_dir));
            % Compute the error for the proportional control. We want to be "on
            % the wall"
            error_rho = dist_line;
            % Set the robot perpendicular to the line, facing the goal
            wall_angle = -sense * pi / 2;
        case WALL
            %% If there is an obstacle use the "moving allong the walls":

            % Compute discontinuities using the first derivative method. This
            % could be improved to be more robust using second derivative
            drho = [0, rho(2:end) - rho(1:end - 1)];
            discontinuity = abs(drho) > 1;

            wallidxs = find(walls);
            % Identify the front wall and its connected walls
            [~, idxs] = sort(rho(walls));
            % This is the index pointing to the front wall, the closest to
            % the robot
            target = wallidxs(idxs(1));
            % In order to smooth the interior corners, we want to detect a
            % second target. This target must be a wall connected to the
            % first
            if smooth_edges

                for k = 2:length(idxs)
                    idx = wallidxs(idxs(k));
                    % In order to check if the second target is connected
                    % to the first, we check that all sensed points between
                    % the targets are 'contacts'. This works in our current
                    % environment as the distance between walls is big and
                    % the range sensor looses contact when there is an
                    % opening in the wall. It also checks if there is a
                    % discontinuity in the range sensor that may indicate
                    % an opening in the wall even if all points in between
                    % are contacts
                    between_idxs = min(target, idx):max(target, idx);
                    is_same_wall = all(contacts(between_idxs)) ...
                        &&~any(discontinuity(between_idxs));
                    % Additionally we filter out points too far from the
                    % goal
                    is_close = rho(idx) <= 2 * wall_dist ...
                        && abs(angdiff(theta(idx), wall_angle)) <= 5 * pi / 8;
                    % If everything checks out we will add the target to
                    % our list of targets, making sure this list is ordered
                    % allways from right to left of the robot
                    if is_same_wall && is_close
                        target(2) = idx;
                        target = sort(target);
                        break;
                    end

                end

            end

            % Now 'target' is a vector of length 1 or 2 that contains the index
            % of the target point or points in the array 'pts' and vectors
            % 'rho' and 'theta'

            %% Model as a moving circle
            if length(target) == 1
                % The moving circle is just parallel to the wall
                r_dir = theta(target);
                error_rho = rho(target) - wall_dist;
                % This are not needed unless we want to plot the moving
                % circle
                if isdebug
                    th0 = wrapToPi(r_dir + pi);
                    r0 = 2 * wall_dist - rho(target(1));
                end

            else
                % The moving circle is defined by two points in the wall. Knowing the
                % radius and two points of the circle we can define its center.
                xa = (pts(1, target(2)) - pts(1, target(1))) / 2;
                ya = (pts(2, target(2)) - pts(2, target(1))) / 2;
                pt_0 = pts(1:2, target(1)) + [xa; ya];
                a = sqrt(xa^2 + ya^2);
                b = sqrt((2 * wall_dist)^2 - a^2);
                center = pt_0 + [-ya; xa] * b / a;
                [th0, r0] = cart2pol(center(1), center(2));
                th0 = th0 + pi / 2;
                % From the center we can extract which is the radial direction, the one
                % that the robot should be moving tangent to
                r_dir = wrapToPi(th0 + pi);
                error_rho = wall_dist - r0;
            end

        otherwise
            error('Unknown state %s.\n', state);
    end

    %% Proportional controllers with limited output to move the robot along the
    %% line and along the wall

    % Once obtained the radial direction, we can calculate the tangent one, the
    % one that the robot should be usually moving to. 
    t_dir = r_dir + sense * pi / 2;
    % Compute the weight of the radial direction in the movement. Depending on
    % the error of the distance to the wall or line, we will add more or less
    % radial correction. Speed will be regulated according to the distance to
    % the goal.
    P = error_rho * KP_R;
    r = max(min(P, 1), -1);
    direction = r * r_dir + (1 - r) * t_dir;
    vel = max(min(dist_goal * KP_VEL, MAX_VEL), -MAX_VEL);
    vel_L = vel * [sin(direction) -cos(direction)];
    leftRightVel = vel_L(1);
    forwBackVel = vel_L(2);

    % Compute the necessary rotational velocity
    error_theta = angdiff(r_dir, wall_angle);
    P = error_theta * KP_ROT_VEL;
    rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);

    %% Debug plot
    % Plot some variables usefull for debugging
    if isdebug
        % Line to goal
        x = linspace(pos(1) - 5.5, pos(1) + 5.5, 100);
        y = l(1) * x + l(2);
        plot(axcart, x, y, 'r-')
        xlim(axcart, pos(1) + [-5.5 5.5])
        ylim(axcart, pos(2) + [-5.5 5.5])
        axis(axcart, 'square')
        hold(axcart, 'on')
        % Position of the robot
        if state == WALL, c = 'b*'; else, c = 'g*'; end
        plot(axcart, pos(1), pos(2), c)
        % Position of the closest point to line
        plot(axcart, ql(1), ql(2), 'k*')
        % Goal position
        plot(axcart, goal_pos(1), goal_pos(2), 'k+')
        % Walls
        [x, y] = pol2cart(theta(contacts) + att - pi / 2, rho(contacts));
        plot(axcart, x + pos(1), y + pos(2), 'b.')
        hold(axcart, 'off')

        % Polar plots
        polarplot(axpolar, theta(contacts), rho(contacts), '.');
        hold(axpolar, 'on')
        rlim(axpolar, [0 5.5]);

        if state == WALL
            % Discontinuities in the range sensor, openings
            polarplot(axpolar, theta(discontinuity), rho(discontinuity), 'bx');
            % Moving circles
            th = linspace(0, 2 * pi);
            r = real(r0 .* cos(th - th0) ...
                + sqrt((2 * wall_dist)^2 - r0^2 .* sin(th - th0).^2));
            polarplot(axpolar, th, r, 'm--');
            r = real(r0 .* cos(th - th0) ...
                + sqrt((wall_dist)^2 - r0^2 .* sin(th - th0).^2));
            polarplot(axpolar, th, r, 'm-');
            % Targets
            polarplot(axpolar, theta(target), rho(target), 'r*');
            % Where the target should be
            polarplot(axpolar, wall_angle, wall_dist, 'go');
        end

        % Where is the robot heading
        polarplot(axpolar, direction, wall_dist, 'gx');
        hold(axpolar, 'off')
    end

end

function varargout = assign_states()
    % Assigns consecutive numeric values to the output arguments
    varargout = num2cell(1:nargout);
end
